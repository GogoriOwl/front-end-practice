
const emptyCard = {
  holderName : "Jane Appleseed",
  cardNumber : "0000 0000 0000 0000",
  cardMMYY : "00",
  cardCVV : "000",
}

window.addEventListener("change", (event) => {

let eventTarget = document.querySelector(`.${event.target.id}`);

  if (event.target.value) {
    eventTarget.innerText = event.target.value;

  } else if(!event.target.value) {
    switch (event.target.id) {
      case "card-mm":
        eventTarget.innerText = emptyCard.cardMMYY;
        break;

        case "card-yy":
        eventTarget.innerText = emptyCard.cardMMYY;
        break;

        case "card-cvv":
          eventTarget.innerText = emptyCard.cardCVV;
          break;

          case "card-number":
        eventTarget.innerText = emptyCard.cardNumber;
        break;

        case "holderName":
          eventTarget.innerText = emptyCard.holderName;
          break;
    }
  }

})
