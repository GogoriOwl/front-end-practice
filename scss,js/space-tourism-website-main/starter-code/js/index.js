
const burgerMenuOpen = document.querySelector(".burger-menu");
const burgerMenuModal = document.querySelector(".burger-menu-modal");
const burgerMenuClose = document.querySelector(".burger-menu-modal__button");

window.addEventListener("click", (event) => {
  if(event.target === burgerMenuOpen) {

    burgerMenuModal.style.display = "block";
    burgerMenuOpen.style.display = "none";

  } else if(event.target === burgerMenuClose) {

    burgerMenuModal.style.display = "none";
    burgerMenuOpen.style.display = "block";

  }

})